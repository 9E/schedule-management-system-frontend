import { User } from "../model/user.model";

export class UserSessionUtil {
  private static readonly USER_KEY = "user";

  public static storeUser(user: User): void {
    localStorage.setItem(this.USER_KEY , JSON.stringify(user));
  }

  public static getStoredUser(): User {
    const userString: string | null = localStorage.getItem(this.USER_KEY);
    if (!userString) {
      throw new Error("Can't get session user because user is not logged in.");
    }

    return JSON.parse(userString);
  }
}
