import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly URL: string = 'http://localhost:8080/api/v1/users';

  constructor(private http: HttpClient) {}

  public createUser(user: User): Observable<User> {
    return this.http.post<User>(this.URL, user);
  }

  public login(user: User): Observable<User> {
    return this.http.post<User>(this.URL + `/login`, user);
  }

  public updateUser(userId: number, user: User): Observable<User> {
    return this.http.put<User>(this.URL + `/${userId}`, user);
  }
}
