import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../model/event.model';

@Injectable({
  providedIn: 'root',
})
export class EventService {
  private readonly USERS_URL: string = 'http://localhost:8080/api/v1/users/';

  constructor(private http: HttpClient) {}

  public getUserEvents(userId: number): Observable<Event[]> {
    return this.http.get<Event[]>(this.USERS_URL + userId + '/events');
  }

  public getEvent(eventId: string): Observable<Event> {
    return this.http.get<Event>(`http://localhost:8080/api/v1/events/${eventId}`);
  }

  public createEvent(userId: number, event: Event): Observable<Event> {
    return this.http.post<Event>(this.USERS_URL + userId + '/events', event);
  }

  public createCustomDaysEvent(userId: number, event: Event): Observable<Event[]> {
    return this.http.post<Event[]>(this.USERS_URL + userId + '/events/customDays', event);
  }

  public updateEvent(eventId: string | undefined, event: Event): Observable<Event> {
    return this.http.put<Event>(`http://localhost:8080/api/v1/events/${eventId}`, event);
  }

  public deleteEvent(eventId: string | undefined): Observable<void> {
    return this.http.delete<void>(`http://localhost:8080/api/v1/events/${eventId}`);
  }

  public getUnprocessedUserEvents(userId: number): Observable<Event[]> {
    return this.http.get<Event[]>(this.USERS_URL + userId + '/events/unprocessed');
  }

  public getProcessedEvents(eventId: string): Observable<Event[]> {
    return this.http.get<Event[]>(`http://localhost:8080/api/v1/events/${eventId}/processed`);
  }
}
