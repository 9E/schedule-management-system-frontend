import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { CalendarComponent } from './components/calendar/calendar.component';

import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import rrulePlugin from '@fullcalendar/rrule';
import { EventFormComponent } from './components/calendar/event-form/event-form.component';
import { EditUserComponent } from './components/calendar/edit-user/edit-user.component';
import { CalculcateTimeComponent } from './components/calendar/calculcate-time/calculcate-time.component'

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin,
  rrulePlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    CalendarComponent,
    EventFormComponent,
    EditUserComponent,
    CalculcateTimeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    FullCalendarModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
