import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserSessionUtil } from 'src/app/util/user-session.util';
import { User } from '../../model/user.model';
import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  loginForm: FormGroup = this.formBuilder.group({
    username: ['', Validators.required],
    password: [''],
  });

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  onLogin(): void {
    this.userService.login(this.loginForm.value).subscribe(
      (user: User) => {
        UserSessionUtil.storeUser(user);
        this.router.navigate(['/calendar']);
      },
      () => {
        alert(`failed to log in`);
      }
    );
  }
}
