import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculcateTimeComponent } from './calculcate-time.component';

describe('CalculcateTimeComponent', () => {
  let component: CalculcateTimeComponent;
  let fixture: ComponentFixture<CalculcateTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculcateTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculcateTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
