import { Component, OnInit } from '@angular/core';
import RRule from 'rrule';
import { Duration } from 'src/app/model/duration.model';
import { Event } from 'src/app/model/event.model';
import { EventService } from 'src/app/service/event.service';
import { UserSessionUtil } from 'src/app/util/user-session.util';

@Component({
  selector: 'app-calculcate-time',
  templateUrl: './calculcate-time.component.html',
  styleUrls: ['./calculcate-time.component.scss'],
})
export class CalculcateTimeComponent implements OnInit {
  eventMap: Map<string, Event> = new Map();
  chosenEventId: string | any;
  duration: Duration = {days:0, hours:0, minutes:0};

  constructor(private eventService: EventService) {}

  ngOnInit(): void {
    const userId: number = UserSessionUtil.getStoredUser().id;
    this.eventService.getUnprocessedUserEvents(userId).subscribe((events) => {
      this.eventMap = this.eventsToMap(events);
    });
  }

  private eventsToMap(events: Event[]): Map<string, Event> {
    const mappedEvents = new Map<string, Event>();
    events.forEach(event => mappedEvents.set(event.id as string, event));
    return mappedEvents;
  }

  onNgModelChange(): void {
    const chosenEvent: Event | undefined = this.eventMap.get(this.chosenEventId);

    if (chosenEvent) {
      const rrule = this.convertToRRule(chosenEvent);
      if (!chosenEvent.isDailyCustomFrequency) {
        if (chosenEvent.allDay) {
          console.log(rrule.count())
          this.duration = { days: rrule.count(), hours: 0, minutes: 0};
        } else {
          const duration = chosenEvent.duration;
          const durationInMinutes = duration.days * 24 * 60 + duration.hours * 60 + duration.minutes;
          const totalDurationInMinutes = durationInMinutes * rrule.count();

          const newDays = Math.floor(totalDurationInMinutes / (24 * 60));
          const newHours = Math.floor((totalDurationInMinutes - (newDays * 24 * 60)) / 60);
          const newMinutes = Math.floor((totalDurationInMinutes - (newDays * 24 * 60) - (newHours * 60)));
          this.duration = {days: newDays, hours: newHours, minutes: newMinutes};
        }
      } else if (chosenEvent.isDailyCustomFrequency) {
          this.eventService.getProcessedEvents(chosenEvent.id as string)
            .subscribe(events => {
              if (chosenEvent.allDay) {
                this.duration = {
                  days: events.reduce((total, event) => total + this.convertToRRule(event).count(), 0),
                  hours: 0,
                  minutes: 0
                }
              } else {
                const totalCount = events.reduce((total, event) => total + this.convertToRRule(event).count(), 0);
                const duration = chosenEvent.duration;
                const durationInMinutes = duration.days * 24 * 60 + duration.hours * 60 + duration.minutes;
                const totalDurationInMinutes = durationInMinutes * totalCount;

                const newDays = Math.floor(totalDurationInMinutes / (24 * 60));
                const newHours = Math.floor((totalDurationInMinutes - (newDays * 24 * 60)) / 60);
                const newMinutes = Math.floor((totalDurationInMinutes - (newDays * 24 * 60) - (newHours * 60)));
                this.duration = {days: newDays, hours: newHours, minutes: newMinutes};
              }
          });
        }
      }
    }

  private durationToTotalMinutes(duration: Duration): number {
    return 5;
  }

  private convertToRRule(event: Event): RRule {
    return new RRule({
      freq: event.rrule.freq,
      dtstart: new Date(event.rrule.dtstart),
      until: new Date(event.rrule.until),
      interval: event.rrule.interval,
      byweekday: event.rrule.byweekday,
    });
  }
}
