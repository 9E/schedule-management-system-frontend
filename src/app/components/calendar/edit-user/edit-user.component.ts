import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/service/user.service';
import { UserSessionUtil } from 'src/app/util/user-session.util';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
})
export class EditUserComponent {
  private ngbModalRef!: NgbModalRef;
  private sessionUser: User = UserSessionUtil.getStoredUser();

  editForm: FormGroup = this.formBuilder.group({
    username: [this.sessionUser.username, Validators.required],
    password: [this.sessionUser.password, Validators.required],
    email: [this.sessionUser.email, Validators.required],
  });

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}

  onSubmit(): void {
    const userId: number = UserSessionUtil.getStoredUser().id;

    this.userService.updateUser(userId, this.editForm.value).subscribe(
      (user: User) => {
        UserSessionUtil.storeUser(user);
        alert(`Sucessfuly updated user`);
        this.ngbModalRef.close();
      },
      (error) => {
        alert('Failed to update user');
      }
    );
  }
}
