import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { EventApi, EventClickArg } from '@fullcalendar/angular';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import RRule, { Frequency, Weekday } from 'rrule';
import { Event } from 'src/app/model/event.model';
import { FrequencyWrapper } from 'src/app/model/frequency-wrapper.enum';
import { EventService } from 'src/app/service/event.service';
import { UserSessionUtil } from 'src/app/util/user-session.util';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss'],
})
export class EventFormComponent implements OnInit {
  ngbModalRef!: NgbModalRef;
  eventClickArg!: EventClickArg;
  mode: string = 'create';
  title: string = 'Create event';

  freqs: Map<FrequencyWrapper, string> = new Map<FrequencyWrapper, string>([
    [FrequencyWrapper.WEEKLY, 'Weekly'],
    [FrequencyWrapper.DAILY, 'Daily'],
    [FrequencyWrapper.DAILY_CUSTOM, 'Daily custom']
  ]);

  eventForm: FormGroup = this.formBuilder.group({
    id: '',
    title: '',
    frequency: '',
    interval: 1,
    dateStart: '',
    timeStart: '',
    allDay: true,
    dateUntil: '',
    duration: this.formBuilder.group({
      days: 0,
      hours: 0,
      minutes: 0,
    }),
    customDays: this.formBuilder.array([this.formBuilder.control(false)]),
    weekdays: this.formBuilder.group({
      monday: false,
      tuesday: false,
      wednesday: false,
      thursday: false,
      friday: false,
      saturday: false,
      sunday: false,
    }),
  });

  get customDays(): FormArray {
    return this.eventForm.get('customDays') as FormArray;
  }

  constructor(
    private formBuilder: FormBuilder,
    private eventService: EventService
  ) {}

  ngOnInit(): void {
    if (this.mode === 'edit') {
      this.title = 'Edit event';
      const frontendEvent: EventApi = this.eventClickArg.event;
      this.eventService.getEvent(frontendEvent.id).subscribe(
        (event: Event) => {
          let formData = this.buildFormEventFromResponse(event);
          if (formData.isDailyCustomFrequency) {
            formData.frequency = FrequencyWrapper.DAILY_CUSTOM;
          }
          this.eventForm.patchValue(formData);
        },
        (error) => console.log(error)
      );
    }

    this.onIntervalChanged();
  }

  private onIntervalChanged() {
    this.eventForm.get('interval')?.valueChanges.subscribe(
      (value) => {
        const oldControls = this.customDays.controls.slice(0, value);
        this.customDays.clear();
        oldControls.forEach(control => this.customDays.push(control));

        for (let i = oldControls.length; i < value; ++i) {
          const formControl = this.formBuilder.control(false);
          this.customDays.push(formControl);
        }
      }
    );
  }

  public onCreate(): void {
    const eventRequest: Event = this.buildEventRequest();
    const userId: number = UserSessionUtil.getStoredUser().id;

    if (!eventRequest.isDailyCustomFrequency) {
        this.eventService.createEvent(userId, eventRequest).subscribe(
          (event) => this.ngbModalRef.close(), // TODO: it doesn't feel good to close a modal from within a callback
          (error) => {
            alert('Failed to create an event.');
            console.log(error);
          }
        );
    } else {
      this.eventService.createCustomDaysEvent(userId, eventRequest).subscribe(
        (event) => this.ngbModalRef.close(),
        (error) => {
          alert('Failed to create an event.');
          console.log(error);
        }
      );
    }
  }

  public onUpdate(): void {
    const eventRequest: Event = this.buildEventRequest();
    this.eventService.updateEvent(eventRequest.id, eventRequest).subscribe(
      (event) => this.ngbModalRef.close(event),
      (error) => {
        alert('Failed to update an event.');
        console.log(error);
      }
    );
  }

  public onDelete(): void {
    const eventRequest: Event = this.buildEventRequest();
    this.eventService.deleteEvent(eventRequest.id).subscribe(
      () => this.ngbModalRef.close(),
      (error) => {
        alert('Failed to delete an event.');
        console.log(error);
      }
    );
  }

  private buildEventRequest(): Event {
    const form = this.eventForm.value;
    let isDailyCustomFrequency: boolean = form.frequency === FrequencyWrapper.DAILY_CUSTOM;
    let frequency: Frequency = isDailyCustomFrequency ? Frequency.DAILY : form.frequency;

    return {
      id: form.id,
      title: form.title,
      allDay: form.allDay,
      rrule: {
        freq: frequency,
        interval: form.interval,
        byweekday: this.getFormWeekDaysArray(),
        dtstart: new Date(form.dateStart + (!form.allDay && form.timeStart ? `T${form.timeStart}` : '')),
        until: new Date(form.dateUntil),
      },
      duration: !form.allDay ? form.duration : null,
      isDailyCustomFrequency: isDailyCustomFrequency,
      customDays: form.frequency === FrequencyWrapper.DAILY_CUSTOM ? form.customDays : null
    };
  }

  private getFormWeekDaysArray(): Weekday[] | any {
    const weekdaysMap = new Map<string, Weekday>([
      ['monday', RRule.MO],
      ['tuesday', RRule.TU],
      ['wednesday', RRule.WE],
      ['thursday', RRule.TH],
      ['friday', RRule.FR],
      ['saturday', RRule.SA],
      ['sunday', RRule.SU],
    ]);

    const weekdaysObj = this.eventForm.value.weekdays;

    return Object.keys(weekdaysObj)
      .filter(weekday => weekdaysObj[weekday])
      .map(weekday => weekdaysMap.get(weekday)?.weekday);
  }

  private buildFormEventFromResponse(event: Event | any) {
    const startDate = new Date(event.rrule?.dtstart);
    const hoursString = `${startDate.getHours()}`.padStart(2, '0');
    const minutesString = `${startDate.getMinutes()}`.padStart(2, '0');
    const dateUntil = new Date(event.rrule.until);

    return {
      id: event.id,
      title: event.title,
      frequency: event.isDailyCustomFrequency ? FrequencyWrapper.DAILY_CUSTOM : event.rrule.freq,
      interval: event.rrule.interval,
      weekdays: this.weekdayNumbersToFormObject(event.rrule.byweekday),
      dateStart: this.getFormattedDateString(startDate),
      timeStart: `${hoursString}:${minutesString}`,
      allDay: event.allDay,
      dateUntil: this.getFormattedDateString(dateUntil),
      duration: event.duration,
      isDailyCustomFrequency: event.isDailyCustomFrequency,
      customDays: event.customDays,
    };
  }

  private getFormattedDateString(date: Date) {
    const year = date.getFullYear();
    const month = `${date.getMonth() + 1}`.padStart(2, '0');
    const day = `${date.getDate()}`.padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  private weekdayNumbersToFormObject(weekdaysNumbers: number[] | undefined) {
    return {
      monday: weekdaysNumbers?.includes(0),
      tuesday: weekdaysNumbers?.includes(1),
      wednesday: weekdaysNumbers?.includes(2),
      thursday: weekdaysNumbers?.includes(3),
      friday: weekdaysNumbers?.includes(4),
      saturday: weekdaysNumbers?.includes(5),
      sunday: weekdaysNumbers?.includes(6),
    }
  }
}
