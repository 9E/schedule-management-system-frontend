import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  CalendarOptions,
  EventClickArg,
  EventHoveringArg
} from '@fullcalendar/angular';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { right } from '@popperjs/core';
import RRule from 'rrule';
import { Event } from 'src/app/model/event.model';
import { EventService } from 'src/app/service/event.service';
import { UserSessionUtil } from 'src/app/util/user-session.util';
import { CalculcateTimeComponent } from './calculcate-time/calculcate-time.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EventFormComponent } from './event-form/event-form.component';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  constructor(
    private modalService: NgbModal,
    private eventService: EventService,
    private router: Router
  ) {}

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    eventTimeFormat: {
      hour: 'numeric',
      minute: '2-digit',
      hour12: false,
    },
    eventMouseEnter: (arg: EventHoveringArg) => {
      arg.el.style.cursor = 'pointer';
    },
    eventMouseLeave: (arg: EventHoveringArg) => {
      arg.el.style.cursor = '';
    },
    eventClick: (arg: EventClickArg) => {
      const modalRef = this.modalService.open(EventFormComponent);
      modalRef.componentInstance.eventClickArg = arg;
      modalRef.componentInstance.mode = 'edit';
      modalRef.componentInstance.ngbModalRef = modalRef;
      modalRef.result.then(() => this.ngOnInit(), () => {});
    },
    eventBorderColor: '#002346',
    customButtons: {
      addEventButton: {
        text: 'Add Event',
        click: () => {
          const modalRef = this.modalService.open(EventFormComponent);
          modalRef.componentInstance.ngbModalRef = modalRef;
          modalRef.result.then(() => this.ngOnInit(), () => {});
        }
      },
      calculateTimeButton: {
        text: 'Calculate time',
        click: () => {
          const modalRef = this.modalService.open(CalculcateTimeComponent);
          modalRef.componentInstance.ngbModalRef = modalRef;
          modalRef.result.then(() => this.ngOnInit(), () => {});
        }
      }
    },
    headerToolbar: {
      center: 'addEventButton',
      end: 'calculateTimeButton today prev,next'
    }
  };

  ngOnInit(): void {
    const userId: number = UserSessionUtil.getStoredUser().id;
    this.eventService.getUserEvents(userId).subscribe(
      (events: Event[] | any) => {
        this.calendarOptions.events = events;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  onLogout(): void {
    this.router.navigate(['']);
  }

  onEditProfile(): void {
    const editProfileModalRef = this.modalService.open(EditUserComponent);
    editProfileModalRef.componentInstance.ngbModalRef = editProfileModalRef;
    editProfileModalRef.result.then(() => this.ngOnInit(), () => {});
  }
}
