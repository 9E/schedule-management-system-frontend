import { Frequency } from "rrule";

export interface RRuleDTO {
  freq: Frequency;
  interval: number;
  byweekday?: number[];
  dtstart: Date;
  until: Date;
}
