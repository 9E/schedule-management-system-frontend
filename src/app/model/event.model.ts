import { Duration } from "./duration.model";
import { RRuleDTO } from "./rrule.dto";

export interface Event {
  id?: string;
  title: string;
  allDay: boolean;
  rrule: RRuleDTO;
  duration: Duration;
  isDailyCustomFrequency?: boolean;
  customDays?: boolean[];
}
