import { Frequency } from "rrule";

export enum FrequencyWrapper {
  YEARLY = Frequency.YEARLY,
  MONTHLY = Frequency.MONTHLY,
  WEEKLY = Frequency.WEEKLY,
  DAILY = Frequency.DAILY,
  HOURLY = Frequency.HOURLY,
  MINUTELY = Frequency.MINUTELY,
  SECONDLY = Frequency.SECONDLY,
  DAILY_CUSTOM = 123
}
